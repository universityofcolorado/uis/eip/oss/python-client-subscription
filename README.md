# Python GraphQL Examples

## Getting Started

Use `python3 --version` to find out which version of Python you're running. (Tested on 3.9.6)
Use `python3 -m pip --version` to find out which version of pip you're running. (Tested on 23.2.1)
Use `python3 -m pip install gql websockets oauthlib requests requests_oauthlib requests_toolbelt` to install the dependencies.

## Resources

- [GraphQL](https://graphql.org/)
- [GraphQL Python](https://graphql.org/community/tools-and-libraries/?tags=python#python)
- [Python OauthLib](https://oauthlib.readthedocs.io/en/latest/oauth2/oauth2.html)

## If you get this error message:

`certificate verify failed: unable to get local issuer certificate (_ssl.c:1045)`

Check out this solution: https://stackoverflow.com/questions/52805115/certificate-verify-failed-unable-to-get-local-issuer-certificate

