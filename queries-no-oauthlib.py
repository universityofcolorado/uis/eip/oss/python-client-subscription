from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
from requests.auth import HTTPBasicAuth
import logging, os, requests

logging.basicConfig(level=logging.DEBUG)

# Set the OAuth2 provider URL and client credentials
provider_url = "https://pngtst.qa.cu.edu/as/token.oauth2"
client_id = os.getenv('CLIENT_ID', "your-client-id")
client_secret = os.getenv('CLIENT_SECRET', "your-client-secret")

def get_access_token(url, client_id, client_secret):

    response = requests.post(
        url,
        data={"grant_type": "client_credentials"},
        auth=HTTPBasicAuth(client_id, client_secret)
    )
    print(response)
    return response.json()["access_token"]

token = get_access_token(provider_url, client_id, client_secret)

print(token)

payload = """
    query about {
        about {
            version
            commit
        }
    }
"""

transport = RequestsHTTPTransport(
    url='https://odin-tst.qa.cu.edu/api/',
    headers={
        'Authorization': 'Bearer ' + token
    }
)

client = Client(
    transport=transport,
    fetch_schema_from_transport=False,
)

query = gql(payload)

result = client.execute(query)
print(result)
