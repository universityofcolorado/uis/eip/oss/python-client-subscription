from requests.auth import HTTPBasicAuth
import logging, os, requests, json

logging.basicConfig(level=logging.DEBUG)

# Set the OAuth2 provider URL and client credentials
provider_url = "https://pngtst.qa.cu.edu/as/token.oauth2"
client_id = os.getenv('CLIENT_ID', "your-client-id")
client_secret = os.getenv('CLIENT_SECRET', "your-client-secret")

def get_access_token(url, clientId, clientSecret):

    response = requests.post(
        url,
        data={"grant_type": "client_credentials"},
        auth=HTTPBasicAuth(clientId, clientSecret)
    )
    print(response)
    return response.json()["access_token"]

token = get_access_token(provider_url, client_id, client_secret)

print(token)

query = """
    query about {
        about {
            version
            commit
        }
    }
"""

api_url = "https://odin-tst.qa.cu.edu/api/"

headers={
    "Content-Type": "application/json",
    'Authorization': 'Bearer ' + token
}

# Make the POST request
response = requests.post(api_url, headers=headers, data=json.dumps({"query": query}))

# Print the response
print(response.json())
