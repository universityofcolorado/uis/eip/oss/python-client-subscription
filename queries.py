from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import HTTPBasicAuth
from requests_oauthlib import OAuth2Session
import logging, os

logging.basicConfig(level=logging.DEBUG)

# Set the OAuth2 provider URL and client credentials
provider_url = "https://pngtst.qa.cu.edu/as/authorization.oauth2"
client_id = os.getenv('CLIENT_ID', "your-client-id")
client_secret = os.getenv('CLIENT_SECRET', "your-client-secret")

# Create a BackendApplicationClient object
client = BackendApplicationClient(client_id=client_id)

# Create an OAuth2Session object
oauth = OAuth2Session(client=client)

# Get the access token
token = oauth.fetch_token(
    token_url="https://pngtst.qa.cu.edu/as/token.oauth2",
    auth=HTTPBasicAuth(client_id, client_secret)
)

# Print the access token
print(token["access_token"])


payload = """
    query about {
        about {
            version
            commit
        }
    }
"""

transport = RequestsHTTPTransport(
    url='https://odin-tst.qa.cu.edu/api/',
    headers={
        'Authorization': 'Bearer ' + token["access_token"]
    }
)

client = Client(
    transport=transport,
    fetch_schema_from_transport=False,
)

query = gql(payload)

result = client.execute(query)
print(result)
