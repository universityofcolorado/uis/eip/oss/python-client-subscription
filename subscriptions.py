# Description: This script demonstrates how to subscribe to a GraphQL subscription using the gql library.
from gql import gql, Client
from gql.transport.websockets import WebsocketsTransport
from oauthlib.oauth2 import BackendApplicationClient
from requests.auth import HTTPBasicAuth
from requests_oauthlib import OAuth2Session
import logging, os

logging.basicConfig(level=logging.DEBUG)

# Set the OAuth2 provider URL and client credentials
provider_url = "https://pngtst.qa.cu.edu/as/authorization.oauth2"
client_id = os.getenv('CLIENT_ID', "your-client-id")
client_secret = os.getenv('CLIENT_SECRET', "your-client-secret")

# Create a BackendApplicationClient object
client = BackendApplicationClient(client_id=client_id)

# Create an OAuth2Session object
oauth = OAuth2Session(client=client)

# Get the access token
token = oauth.fetch_token(
    token_url="https://pngtst.qa.cu.edu/as/token.oauth2",
    auth=HTTPBasicAuth(client_id, client_secret)
)

# Print the access token
print(token["access_token"])

payload = """
    subscription {
        timer(interval:20){ 
            localDateTime
        }
    }
"""

# Sec-WebSocket-Protocol: graphql-ws
transport = WebsocketsTransport(
    url='wss://odin-tst.qa.cu.edu/api/subscriptions',
    ping_interval=10,
    headers={
        'Sec-WebSocket-Protocol': 'graphql-ws',
        'Authorization': 'Bearer ' + token["access_token"]
    }
)

client = Client(
    transport=transport,
    fetch_schema_from_transport=False,
)

query = gql(payload)

for result in client.subscribe(query):
    print(result)
